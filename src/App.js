import React, { useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

// pages
import Home from './pages/Home';
import Rent from './pages/Rent';

// styling
import GlobalStyle from './GlobalStyle';
import { gapi } from 'gapi-script'

const App = () => {

    return ( <
        Router >
        <
        Routes >
        <
        Route exact path = '/'
        element = { < Home / > }
        /> <
        Route path = '/rent'
        element = { < Rent / > }
        /> < /
        Routes >

        <
        GlobalStyle / >
        <
        /Router>
    );
}

export default App;