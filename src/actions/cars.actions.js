import { getData } from '../services/cars.services'
import {
    FETCH_CARS_SUCCESS,
    FETCH_CARS_LOADING,
    FETCH_CARS_FAIL,
    FILTER_CARS
} from './type.actions'


export const fetchData = () => async (dispatch) => {
    try{
        await dispatch({
            type: FETCH_CARS_LOADING
        })

        const cars = await getData()
        
        await dispatch({
            type: FETCH_CARS_SUCCESS,
            payload: { cars }
        })
    } catch(err){
        await dispatch({
            type: FETCH_CARS_FAIL,
            payload: { errorMessage: err.message }
        })
    }
}

export const filterCars = (type, date, time, passenger) => async (dispatch) => {
    await dispatch({
        type: FILTER_CARS,
        payload: {
            type,
            date,
            time, 
            passenger
        }
    })
}
