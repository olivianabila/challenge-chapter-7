import styled from "styled-components";

import { Row, Container } from 'react-bootstrap'

export const Wrapper = styled(Container)
`
    padding-top: var(--navbar-height) !important;
    max-width: none;
    
`

export const Content = styled(Row)
`
    text-align: center;

    div{
        text-align: initial;

        i {
            color: rgb(223, 153, 71);
        }
    }

    .command-button {
        width: 150px;
    }

    .carousel-control-prev,
    .carousel-control-next {
        position: initial;
        width: 50px;
        height: 50px;
        border-radius: 50%;
        border: solid 1px black;
        cursor: pointer;
        transition: 0.5s;

        &:focus{
            background-color: greenyellow;
        }
    }

    .carousel-control-prev-icon,
    .carousel-control-next-icon {
        color: black;
        font-size: 2rem;
        background-image: none !important;
    }

    .swiper-wrapper{
        padding: 5px 0;
    }

    .swiper-slide{
        background-color: var(--dark-blue-01);
    }
`