import Person1 from '../../images/img_photo.svg'
import Person2 from '../../images/img_photo2.svg'


export const testimonialInfos = [{
    img: {
        src: Person1,
        alt: "person 1"
    },
    desc: `"Terbaik bukan hanya dari segi harga tetapi dari segi kenyamanan dan segi pelayanan dan mobilnya"`,
    name: "Helen XEE 25, Bromo"
}, {
    img: {
        src: Person2,
        alt: "person 2"
    },
    desc: `"Terima Kasih Binar Rental Car sudah menyediakan sewa mobil untuk kebutuhan mendesak"`,
    name: "Steve Jhon 30, Bandung"
}]