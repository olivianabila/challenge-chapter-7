import React, { useRef } from 'react'

// components
import { Swiper, SwiperSlide } from 'swiper/react'
import { Autoplay } from "swiper";
import { Row, Col } from 'react-bootstrap'

// styles
import { Wrapper, Content } from './Testimonial.styles'

// data testimonials
import { testimonialInfos } from './Testimonial.data';

const Testimonial = () => {
    const slider = useRef(null)
    
    const next = () => {
        if (!slider.current) return
        slider.current.swiper.slideNext()
    }

    const prev = () => {
        if (!slider.current) return
        slider.current.swiper.slidePrev()
    }

    return (
        <Wrapper id="testimonial">
            <Content>

                <h2 className="my-2">Testimonial</h2>
                <p className="my-2">Berbagai review positif dari para pelanggan kami</p>

                <Swiper
                    ref={slider}
                    slidesPerView="auto"
                    spaceBetween="5%"
                    slidesPerGroup={1}
                    centeredSlides
                    loop
                    autoplay={{
                        delay: 1000,
                        disableOnInteraction: false,
                    }}
                    scrollbar={{ draggable: true }}
                    modules={[Autoplay]}
                    className="my-5"
                >
                    { testimonialInfos.map(({ img, desc, name }, index) => (
                        <SwiperSlide key={index} className="py-5 px-4" style={{ width: "50%", height: "95%", boxShadow: "0px 0px 5px 0px rgba(0, 0, 0, 0.75)",}}>
                            <Row>
                                <Col md={3} className="d-flex justify-content-center align-items-center">
                                    <img src={img.src} alt={img.alt} />
                                </Col>
                                <Col md={9}>
                                    <i className="fa-solid fa-star mx-1"></i>
                                    <i className="fa-solid fa-star mx-1"></i>
                                    <i className="fa-solid fa-star mx-1"></i>
                                    <i className="fa-solid fa-star mx-1"></i>
                                    <i className="fa-solid fa-star mx-1"></i>
                                    <p className="my-3">{desc}</p>
                                    <strong>{ name }</strong>
                                </Col>
                            </Row>
                        </SwiperSlide>
                    )) }
                </Swiper>
                <div className="command-button d-flex mx-auto justify-content-between my-4">
                    <button onClick={prev} className="carousel-control-prev">
                        <i className="fa-solid fa-angle-left d-flex align-items-center justify-content-center carousel-control-prev-icon"></i>
                    </button>
                    <button onClick={next} className="carousel-control-next">
                        <i className="fa-solid fa-angle-right d-flex align-items-center justify-content-center carousel-control-next-icon"></i>
                    </button>
                </div>
            </Content>
        </Wrapper>
    )
}

export default Testimonial