import styled from "styled-components";

import { Row, Col } from 'react-bootstrap'


export const Wrapper = styled(Row)`
    padding-top: var(--navbar-height) !important;
    width: 90%;
    margin: 0 auto;
`

export const Content = styled(Col)`
    .icon-connect-with-us {
        width: 1.5rem;
        height: 1.5rem;
        margin-right: 5px;
        background-color: var(--dark-blue-04);
        border-radius: 50%;
    }

    .brand{
        width: 100px;
        height: 34px;
        background-color: var(--dark-blue-04);
        
    }
`



