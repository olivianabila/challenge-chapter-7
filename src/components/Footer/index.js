import React from 'react'

// components
import { Link } from 'react-router-dom'

// styles
import { Wrapper, Content } from './Footer.styles'

const Footer = () => {
    return ( <
        Wrapper >
        <
        Content md = { 3 } >
        <
        p className = "text-tipis" > Tanjung Barat, Jakarta Selatan < /p> <
        p className = "text-tipis" > olivianabila81 @gmail.com < /p> <
        /Content> <
        Content md = { 1 } > < /Content> <
        Content md = { 2 } >
        <
        p > < Link to = "/"
        className = "text-reset" > Our services < /Link></p >
        <
        p > < Link to = "/"
        className = "text-reset" > Why Us < /Link></p >
        <
        p > < Link to = "/"
        className = "text-reset" > Testimonial < /Link></p >
        <
        p > < Link to = "/"
        className = "text-reset" > FAQ < /Link></p >
        <
        /Content> <
        Content md = { 1 } > < /Content> <
        Content md = { 3 } >
        <
        p > Connect with us < /p> <
        div className = "icon-connect d-flex my-1" >
        <
        Link to = "/"
        className = "d-flex justify-content-center align-items-center icon-connect-with-us" >
        <
        i className = "fa-brands fa-facebook-f text-light" > < /i> <
        /Link> <
        Link to = "/"
        className = "d-flex justify-content-center align-items-center icon-connect-with-us" >
        <
        i className = "fa-brands fa-instagram text-light" > < /i> <
        /Link> <
        Link to = "/"
        className = "d-flex justify-content-center align-items-center icon-connect-with-us" >
        <
        i className = "fa-brands fa-twitter text-light" > < /i> <
        /Link> <
        Link to = "/"
        className = "d-flex justify-content-center align-items-center icon-connect-with-us" >
        <
        i className = "fa-solid fa-envelope text-light" > < /i> <
        /Link> <
        Link to = "/"
        className = "d-flex justify-content-center align-items-center icon-connect-with-us" >
        <
        i className = "fa-brands fa-twitch text-light" > < /i> <
        /Link> <
        /div> <
        /Content> <
        Content md = { 2 } >
        <
        p > Copyright Binar 2022 < /p> <
        div className = "brand my-1" > < /div> <
        /Content> <
        /Wrapper>
    )
}

export default Footer