import styled from "styled-components";

export const Wrapper = styled.div`
    width: 82.5%;
    margin: 0 auto;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(350px, 1fr));
    grid-gap: 2rem;
`
