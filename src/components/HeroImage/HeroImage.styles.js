import styled from "styled-components";

import { Container, Row, Col, Button } from 'react-bootstrap'


export const Wrapper = styled(Container)`
    background-color: var(--dark-blue-01);
    padding-top: var(--navbar-height) !important;
    justify-content: space-between;
    max-width: none;
`

export const Content = styled(Row)`
`

export const Description = styled.div`
    padding-left: 5vw;
    width: 87.5%;
`

export const Column = styled(Col)`
    margin: auto;
    
    button{
        background-color: var(--limegreen-04) !important;
        border: none;
        outline: none;
    }
`

export const StartRentBtn = styled(Button)`
    background-color: var(--limegreen-04) !important;
    border: none;
    outline: none;
`
