import React from 'react'

// components
import { Link } from 'react-router-dom'
import HeroImagePhoto from '../../images/img_car.svg'

// styles
import { Wrapper, Content, Column, StartRentBtn, Description } from './HeroImage.styles'

const HeroImage = ({ home_page }) => {
    return ( <
        Wrapper >
        <
        Content >
        <
        Column md = { 6 } >
        <
        Description >
        <
        h1 > Sewa & amp; Rental Mobil Terbaik di Kawasan Jakarta Selatan < /h1> <
        p className = "text-tipis" > Selamat datang di Binar Car Rental.Kami menyediakan mobil terbaik dengan harga terjangkau.Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam. < /p> { home_page && < Link to = "/rent" > < StartRentBtn variant = "success"
            className = "text-light" > Mulai Sewa Mobil < /StartRentBtn></Link > } <
        /Description> <
        /Column> <
        Column md = { 6 } >
        <
        img src = { HeroImagePhoto }
        alt = "hero"
        className = "h-100 w-100 " / >
        <
        /Column> <
        /Content> <
        /Wrapper>
    )
}

export default HeroImage