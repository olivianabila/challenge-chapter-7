import styled from "styled-components";

import { Card } from 'react-bootstrap'


export const Wrapper = styled(Card)`
    width: ${({ width }) => width};
    height: ${({ height }) => height};
    margin: 25px auto;
    display: flex;
    justify-content: space-evenly;
    flex-direction: column;
`

export const Content = styled(Card.Body)`
    display: flex;
    justify-content: space-evenly;
    flex-direction: column;
    width: 75%;
    margin: 0 auto;
    padding: 0;

    .main-image{
        width: 270px;
        height: 160px;
        object-fit: cover;
        margin: 0 auto;
    }

    .icon{
        width: 32px;
        height: 32px;
    }
    
    button{
        width: 100%;
        
    }
`
