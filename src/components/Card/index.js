import React from 'react'

// styles
import { Wrapper, Content } from './Card.styles'

const Card = ({ styles, children }) => {
    return (
        <Wrapper {...styles}>
            <Content>
                { children }
            </Content>
        </Wrapper>
    )
}

export default Card