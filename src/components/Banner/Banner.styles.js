import styled from "styled-components";

import { Button } from 'react-bootstrap'

export const Wrapper = styled.div`
    
    padding-top: var(--navbar-height) !important;
`

export const Content = styled.div`
    width: 90%;
    height: 90%;
    padding: 7.5rem 0;
    text-align: center;
    border-radius: 25px;
    background-color: var(--dark-blue-04);
`

export const Title = styled.h2`
    width: 75%;
    color: white;
`
export const Description = styled.p`
    width: 50%;
    color: white;
    font-weight: 100;
    padding-bottom: 50px;
`
export const RentBtn = styled(Button)`
    
`

