import React from 'react'

// styles
import { Wrapper, Content, Title, Description, RentBtn } from './Banner.styles'

const Banner = () => {
    return ( <
        Wrapper >
        <
        Content className = "mx-auto" >
        <
        Title className = "mx-auto" > Sewa Mobil di Jakarta Selatan Sekarang < /Title> <
        Description className = "mx-auto" >
        Sewa & Rental Mobil terbaik di Jakarta. <
        /Description> <
        RentBtn variant = "success"
        className = "mx-auto" > Mulai Sewa Mobil < /RentBtn> <
        /Content> <
        /Wrapper>
    )
}

export default Banner