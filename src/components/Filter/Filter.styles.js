import styled from "styled-components";

import { Button, Row, Col } from 'react-bootstrap'

export const Wrapper = styled(Row)`
    position: relative;
    background-color: white;
    width: 82.5%;
    transform: translateY(-50%);
    z-index: 1000;
    *{
        transition: transform 0.25s
    }
    &.medium-shadow{
        box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
        -webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
        -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
    }

    &.full-shadow{
        box-shadow: 0px 0px 0px 1000vmax rgba(0, 0, 0, 0.75);
       
        user-select: none;
    }

    
`

export const Content = styled(Col)`
    label{
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;

        
    }

    i{
        position: absolute;
        bottom: 20%;
        right: 12.5%;
        z-index: 100;
        background-color: white;
        
        &.active{
            transform: rotate(180deg);
        }
    }
`

export const Dropdown = styled.input`
    position: relative;
    width: 100%;
    z-index: 10;
    color: #8A8A8A;
    margin-top: 5px;
    border: 1px solid #d0d0d0;
    border-radius: 2px;
    padding: 5px 12px;
    padding-right: 25px;
    outline: none;
    cursor: pointer;
`

export const DatePicker = styled.input`
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
    width: calc(100% - 1.5rem);
    border: none;
`

export const Options = styled.div`
    position: absolute;
    top: 100%;
    left: 50%;
    width: 100%;
    transform: translateX(-50%);
    height: 75px;
    overflow-y: auto;
    display: none;
    border-radius: 2px;
    box-shadow: 0 2px 16px rgba(0, 0, 0, 0.1);
    width: calc(100% - 24px) !important;
    z-index: 1000;

    &.show{
        display: block;
    }
    ::-webkit-scrollbar {
        width: 2px;
    }
    
    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: rgb(0,0,0); 
        
    }
`


export const Option = styled.p`
    background-color: white;
    width: 100%;
    padding: 5px 12px;
    margin: 0;
    cursor: pointer;
    border: 1px solid rgba(0,0,0,0.125);
    transition: 0.25s;
    :hover{
        background-color: #c9e7ca;
        font-weight: 700;
        color: #5cb85f;
    }

    ::after{
        content: attr(data-ext);
        position: absolute;
        right: 5px;
        top: 50%;
        transform: translateY(-50%);
    }

`

export const SearchBtn = styled(Button)`

`
