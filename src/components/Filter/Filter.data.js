export const availableDriver = {
    options: [
        {
            text: "Dengan Driver",
            value: "Dengan Driver"
        }, {
            text: "Tanpa Driver (Lepas Kunci)",
            value: "Tanpa Driver (Lepas Kunci)"
        }
    ],
    target: "type"
}

export const availableHour = {
    options: [
        {
            text: "01:00",
            value: "01:00"
        },
        {
            text: "02:00",
            value: "02:00"
        },
        {
            text: "03:00",
            value: "03:00"
        },
        {
            text: "04:00",
            value: "04:00"
        },
        {
            text: "05:00",
            value: "05:00"
        },
        {
            text: "06:00",
            value: "06:00"
        },
        {
            text: "07:00",
            value: "07:00"
        },
        {
            text: "08:00",
            value: "09:00"
        },
        {
            text: "10:00",
            value: "10:00"
        },
        {
            text: "11:00",
            value: "11:00"
        },
        {
            text: "12:00",
            value: "12:00"
        },
        {
            text: "13:00",
            value: "13:00"
        },
        {
            text: "14:00",
            value: "14:00"
        },
        {
            text: "15:00",
            value: "15:00"
        },
        {
            text: "16:00",
            value: "16:00"
        },
        {
            text: "17:00",
            value: "17:00"
        },
        {
            text: "18:00",
            value: "18:00"
        },
        {
            text: "19:00",
            value: "19:00"
        },
        {
            text: "20:00",
            value: "20:00"
        },
        {
            text: "21:00",
            value: "21:00"
        },
        {
            text: "22:00",
            value: "22:00"
        },
        {
            text: "23:00",
            value: "23:00"
        },
    ],
    ext: "WIB",
    target: "time"
}

export const availablePassenger = {
    options: [
        {
            text: "0",
            value: 0
        }, 
        {
            text: "1",
            value: 1
        }, 
        {
            text: "2",
            value: 2
        }, 
        {
            text: "3",
            value: 3
        }, 
        {
            text: "4",
            value: 4
        }, 
        {
            text: "5",
            value: 5
        }, 
        {
            text: "6",
            value: 6
        }, 
        {
            text: "7",
            value: 7
        }, 
        {
            text: "8",
            value: 8
        }, 
    ],
    ext: "Orang",
    target: "passenger"
}