import React, { useState, useRef, useEffect } from 'react'

// styles
import { Wrapper, Content, Dropdown, DatePicker, Options, Option, SearchBtn } from './Filter.styles'

// data options
import { availableDriver, availableHour, availablePassenger } from './Filter.data'

// redux state management
import { useDispatch } from 'react-redux'
import { filterCars } from '../../actions/cars.actions'

const Filter = () => {
    const [type, setType] = useState("")
    const [date, setDate] = useState("")
    const [time, setTime] = useState("")
    const [passenger, setPassenger] = useState("")

    const container = useRef(null)

    const driverRef = useRef(null)
    const driverIcon = useRef(null)

    const dateRef = useRef(null)
    const inputDate = useRef(null)
    const dateIcon = useRef(null)


    const timeRef = useRef(null)
    const timeIcon = useRef(null)

    const passengerRef = useRef(null)
    const passengerIcon = useRef(null)

    const dispatch = useDispatch()

    const openDropdown = (e) => {
        closeDropdown()
        const target = e.currentTarget.name || e.currentTarget.htmlFor || e.currentTarget.dataset.target
        switch (target){
            case "tipe-driver":
                driverRef.current.classList.add("show")
                driverIcon.current.className = "fa-solid fa-angle-down active"
                containerOn()
                break
            case "tanggal":
                dateRef.current.classList.add("show")
                inputDate.current.showPicker()
                dateIcon.current.className = "fa-solid fa-angle-down active"
                containerOn()
                break
            case "waktu":                
                timeRef.current.classList.add("show")
                timeIcon.current.className = "fa-solid fa-angle-down active"
                containerOn()
                break
            case "orang":
                passengerRef.current.classList.add("show")
                passengerIcon.current.className = "fa-solid fa-angle-down active"
                containerOn()
                break
            default:
                break
        }
    }

    const containerOn = () => {
        container.current.classList.remove("medium-shadow")
        container.current.classList.add("full-shadow")
    }

    const containerOff = (e) => {
        const { dataset } = e.currentTarget
        const value = e.currentTarget.dataset.value || e.currentTarget.value
        
        if (dataset.target === "type"){
            setType(value)
        } else if (dataset.target === "date"){
            setDate(value)
        } else if (dataset.target === "time"){
            setTime(value)
        } else if (dataset.target === "passenger"){
            setPassenger(value)
        }
        closeDropdown()
    }

    const closeDropdown = () => {
        if (container.current){
            container.current.classList.remove("full-shadow")
            container.current.classList.add("medium-shadow")
        }

        if (driverRef.current && driverIcon.current){
            driverRef.current.classList.remove("show")
            driverIcon.current.className = "fa-solid fa-angle-down"
        }
        
        if (dateRef.current && dateIcon.current){
            dateRef.current.classList.remove("show")
            dateIcon.current.className = "fa-regular fa-calendar"
        }
        
        if (timeRef.current && timeIcon.current){
            timeRef.current.classList.remove("show")
            timeIcon.current.className = "fa-regular fa-clock"
        }

        if (passengerRef.current && passengerIcon.current){
            passengerRef.current.classList.remove("show")
            passengerIcon.current.className = "fa-regular fa-user"
        }
    }
    const onSubmit = () => {
        dispatch(filterCars(type, date, time, passenger))
    }

    useEffect(() => {
        window.addEventListener("click", e => {
            if (container.current){
                if (!container.current.contains(e.target)){
                    closeDropdown()
                }
            }
        })
    }, [])


    return (
        <Wrapper ref={container} className="py-5 mx-auto medium-shadow">
            <Content md={1}></Content>
           
            <Content md={2} className="position-relative d-flex flex-column">
                <label htmlFor='tipe-driver' onClick={openDropdown}>Tipe Driver</label>
                <Dropdown 
                    type="text" 
                    name="tipe-driver" 
                    id="tipe-driver" 
                    className='py-2' 
                    placeholder='Pilih Tipe Driver'
                    value={type} 
                    readOnly 
                    onClick={openDropdown} 
                />
                <Options ref={driverRef}>
                    {
                        availableDriver.options.map(({ text, value }) => (
                            <Option key={text} className="position-relative" data-target={availableDriver.target} data-ext={availableDriver.ext} data-value={value} onClick={containerOff}>{text}</Option>
                        ))
                    }
                </Options>
                <i data-target="tipe-driver" ref={driverIcon} className="fa-solid fa-angle-down" onClick={openDropdown}></i>
            </Content>
            
            <Content md={2} className="position-relative d-flex flex-column">
                <label htmlFor='tanggal' onClick={openDropdown}>Tanggal</label>
                <Dropdown 
                    ref={dateRef} 
                    type="text"
                    name="tanggal" 
                    id="tanggal" 
                    className='py-2' 
                    placeholder='Pilih Tanggal' 
                    value={date} 
                    readOnly 
                    onClick={openDropdown}
                />
                <DatePicker 
                    ref={inputDate} 
                    type="date" 
                    name='tanggal' 
                    data-target="date"
                    onChange={containerOff}
                />
                <i data-target="tanggal" ref={dateIcon} className="fa-regular fa-calendar" onClick={openDropdown}></i>
            </Content>
            
            <Content md={2} className="position-relative d-flex flex-column">
                <label htmlFor='waktu' onClick={openDropdown}>Waktu Jemput/Ambil</label>
                <Dropdown 
                    type="text" 
                    name="waktu" 
                    id="waktu" 
                    className='py-2'
                    placeholder='Pilih Waktu'
                    value={time ? `${time} ${availableHour.ext}` : ""}  
                    readOnly 
                    onClick={openDropdown}
                />

                <Options ref={timeRef}>
                    {
                        availableHour.options.map(({ text, value }) => (
                            <Option key={text} className="position-relative" data-target={availableHour.target} data-ext={availableHour.ext} data-value={value} onClick={containerOff}>{text}</Option>
                        ))
                    }
                </Options>
                <i data-target="waktu" ref={timeIcon} className="fa-regular fa-clock" onClick={openDropdown}></i>
            </Content>
            
            <Content md={2} className="position-relative d-flex flex-column">
                <label htmlFor='orang' onClick={openDropdown}>Jumlah Penumpang (optional)</label>
                <Dropdown 
                    type="text" 
                    name="orang" 
                    id="orang" 
                    className='py-2' 
                    placeholder='Pilih Penumpang' 
                    value={passenger ? `${passenger} ${availablePassenger.ext}` : ""} 
                    readOnly 
                    onClick={openDropdown}
                />
                <Options ref={passengerRef}>
                    {
                        availablePassenger.options.map(({ text, value }) => (
                            <Option key={text} className="position-relative" data-target={availablePassenger.target} data-ext={availablePassenger.ext} data-value={value} onClick={containerOff}>{text}</Option>
                        ))
                    }
                </Options>
                <i data-target="orang" ref={passengerIcon} className="fa-regular fa-user" onClick={openDropdown}></i>
            </Content>
            
            <Content md={2} className="position-relative d-flex flex-column-reverse">
                <SearchBtn variant="success" className="py-2" onClick={onSubmit}>Cari Mobil</SearchBtn>
            </Content>
            <Content md={1}></Content>
        </Wrapper>
    )
}

export default Filter