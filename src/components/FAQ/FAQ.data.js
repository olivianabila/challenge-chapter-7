export const AccordionList = [{
        question: "Apa saja syarat yang dibutuhkan?",
        answer: "Syarat yang harus disiapkan NPWP,KTP dan SIM. "
    },
    {
        question: "Berapa hari minimal sewa mobil lepas kunci?",
        answer: "Minimal untuk mobil lepas kunci di daerah Jakarta adalah 2 hari penyewaan, di luar Jakarta minimal 3 hari penyewaan"
    },
    {
        question: "Berapa hari sebelumnya sebaiknya booking sewa mobil?",
        answer: "Dianjurkan untuk booking 12 jam sebelum serah terima unit karena akan dilakukan verifikasi data terlebih dahulu"
    },
    {
        question: "Apakah Ada biaya antar-jemput?",
        answer: "Tidak ada biaya untuk antar jemput di sekitaran Jakarta selatan, selain di Jakarta Selatan diPerkenakan Biaya antar jemput."
    },
    {
        question: "Bagaimana jika terjadi kecelakaan?",
        answer: "Jika kecelakan terjadi karena penyewa maka penyewa harus mengganti kerusakan, jika kerusakan terjadi karena mengalami masalah pada pemakaian maka team kami akan mengantarkan mobil untuk pengantinya sesuai perjanjian. "
    },
]