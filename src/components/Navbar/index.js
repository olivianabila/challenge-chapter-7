import React from 'react'

import { Wrapper, Content, ToggleBtn, Link, Brand } from './Navbar.styles'



const Navigation = () => {


    return ( <
        Wrapper className = "mx-auto"
        fixed = "top"
        expand = "lg" >
        <
        Content id = "basic-navbar-nav" >
        <
        nav class = "navbar navbar-expand-lg navbar-light" >
        <
        div class = "container" >
        <
        div class = "collapse navbar-collapse"
        id = "navbarNavAltMarkup" >
        <
        div class = "navbar-nav ml-auto" >
        <
        a class = "nav-item nav-link"
        href = "#ourservice" > Our Service < /a> <
        a class = "nav-item nav-link"
        href = "#whyus" > Why Us < /a> <
        a class = "nav-item nav-link"
        href = "#testi" > Testimonial < /a> <
        a class = "nav-item nav-link"
        href = "#faq" > FAQ < /a> <
        a class = "nav-item  btn btn-success tombol"
        href = "#" > register < /a> <
        /div> <
        /div> <
        /div> <
        /nav>


        <
        /Content> <
        /Wrapper>
    )
}

export default Navigation