import styled from "styled-components";

import { Navbar, Nav, Button } from 'react-bootstrap'


export const Wrapper = styled(Navbar)`
    background-color: var(--dark-blue-01);
    min-height: var(--navbar-height);
    padding-left: 5%;
    padding-right: 5%;
    justify-content: space-between;
    z-index: 10000;
`

export const Content = styled(Navbar.Collapse)`
    flex-grow: initial;

    .dropdown-toggle{
        color: white;
        &[aria-expanded=true]{
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        
    }

    .dropdown-menu.show{
        background-color: transparent;
        padding: 0;
        width: 100%;
        margin: 0;
        min-width: initial;
        border: none;

        a{
            padding: 0;
            background-color: transparent;
            width: 100%;
            border-bottom-left-radius: 0.25rem;
            border-bottom-right-radius: 0.25rem;

            button{
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }
        }
    }
    
`
export const ToggleBtn = styled(Navbar.Toggle)`

`

export const Link = styled(Nav.Link)`
    
`

export const RegisterBtn = styled(Button)`
    background-color: var(--limegreen-04) !important;
    border: none;
    outline: none;
`

export const Brand = styled(Navbar.Brand)`
    width: 100px;
    height: 34px;
    background-color: var(--dark-blue-04);
`


