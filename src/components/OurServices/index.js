import React from 'react'

// components
import OurServicesPhoto from '../../images/img_service.svg'
import CheckList from '../../images/checklist.svg'

// styles
import { Wrapper, Content } from './OurServices.styles'

// data services
import { servicesList } from './OurServices.data'

const OurServices = () => {
    return ( <
        Wrapper id = "our-services"
        className = "py-5" >
        <
        Content md = { 6 } >
        <
        img src = { OurServicesPhoto }
        alt = "our services"
        className = 'our-service-image' / >
        <
        /Content>

        <
        Content md = { 6 } >
        <
        h2 > Best Car Rental
        for any kind of trip in Jakarta Selatan! < /h2> <
        p > Sewa mobil di Jakarta Selatan bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain,
        kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding,
        meeting, dll. < /p> <
        ul className = "fa-ul" > {
            servicesList.map((service) => (

                <
                li key = { service }
                className = "d-flex align-items-center my-4" >
                <
                span className = "fa-li text-align-initial" >
                <
                img src = { CheckList }
                className = "checklist"
                alt = "checklist" / >
                <
                /span> <
                p className = "mx-3 my-0" > { service } < /p> < /
                li >
            ))
        } <
        /ul> < /
        Content > <
        /Wrapper>      
    )
}

export default OurServices