import api from '../config/api'

export const getData = async () => {
    const response = await api.get("/fnurhidayat/probable-garbanzo/main/data/cars.min.json", )
    const cars = response.data
    
    localStorage.setItem("cars", JSON.stringify(cars))

    return cars
}

