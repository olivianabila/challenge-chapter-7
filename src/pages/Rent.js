import React, { useEffect } from 'react'

import UserImg from '../images/icon_user.svg'
import SettingImg from '../images/icon_setting.svg'
import CalendarImg from '../images/icon_calender.svg'

// components
import Navbar from '../components/Navbar'
import HeroImage from '../components/HeroImage'
import Footer from '../components/Footer'
import Filter from '../components/Filter'
import Card from '../components/Card'
import Spinner from '../components/Spinner'

// import api from '../config/config'
import Grid from '../components/Grid'
import { Button } from 'react-bootstrap'

import { fetchData } from '../actions/cars.actions'
import { useDispatch, useSelector } from 'react-redux'

const Rent = () => {
    const { loading, error, errorMessage, filteredCars } = useSelector(state => state.cars)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchData())
    }, [dispatch])

    
    return (
        <>
            <Navbar />
            <HeroImage />
            <Filter />
            
            <Grid>
                {
                    !error && filteredCars?.map(car => (
                        <Card key={car.id} styles={{width: "350px", height: "586px"}}>
                            <img src={car.image.replace("./", "https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/public/")} alt={car.model} className="main-image" />
                            <h2>{ car.model } / { car.manufacture }</h2>
                            <strong>Rp. { car.rentPerDay.toLocaleString() } / hari</strong>
                            <p>{ car.description }</p>

                            <div className="d-flex">
                                <img src={UserImg} alt="passenger icon" className='icon mx-2' />
                                <p className="d-flex  align-items-center m-0">{car.capacity} orang</p>
                            </div>
                           
                            <div className="d-flex">
                                <img src={SettingImg} alt="setting icon" className='icon mx-2' />
                                <p className="d-flex align-items-center m-0">{car.transmission}</p>
                            </div>

                            <div className="d-flex">
                                <img src={CalendarImg} alt="calendar icon" className='icon mx-2' />
                                <p className="d-flex  align-items-center m-0">{String(new Date(car.availableAt))}</p>
                            </div>

                            <Button variant="success" className="text-light py-2">Pilih Mobil</Button>
                        </Card> 
                    ))
                }

                {
                    loading && <Spinner />
                }

                {
                    error && <div>{errorMessage}</div>
                }
            </Grid>
            <Footer />

        </>
    )
}

export default Rent