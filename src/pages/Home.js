import React from 'react'

// components
import Navbar from '../components/Navbar'
import HeroImage from '../components/HeroImage'
import OurServices from '../components/OurServices'
import WhyUs from '../components/WhyUs'
import Testimonial from '../components/Testimonial'
import Banner from '../components/Banner'
import FAQ from '../components/FAQ'
import Footer from '../components/Footer'

const Home = () => (
    <>
        <Navbar />
        <HeroImage home_page />
        <OurServices />
        <WhyUs />
        <Testimonial />
        <Banner />
        <FAQ />
        <Footer />
    </>
)

export default Home
