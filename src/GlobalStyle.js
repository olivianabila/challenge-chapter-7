import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    :root{
        --dark-blue-01: #cfd4ed;
        --dark-blue-02: #aeb7e1;
        --dark-blue-04: #0d28a6;
        --red: #D00C1A;
        --success: #73ca5c;
        --neutral-02: #d0d0d0;
        --neutral-03: #8a8a8a;
        --neutral-04: #3c3c3c;
        --neutral-05: #151515;

        --alert: #FA2C5A;
        --limegreen-04: #5cb85f;

        --big-size: 1024px;
        --navbar-height: 70px;
    }
    html{
        scroll-behavior: smooth;
    }

    *{
        margin: 0;
        padding: 0;
        font-size: 14px;
        box-sizing: border-box;
        font-family: Helvetica, sans-serif;
    }

    h1{
        font-size: 2rem;
        line-height: 36px;
        font-weight: 700;
    }

    a{
        text-decoration: none !important;
        color: black !important;
    }


    .text-tipis {
        font-weight: 300;
        font-size: 15px;
    }

    .text-small {
        font-size: 13px;
    }

`
export default GlobalStyle
