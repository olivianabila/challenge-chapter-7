Download all the package and it's dependencies
```bash
Yarn install
```

## Run The Server

Run the following command to start the server

```bash
  Yarn start
```

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes. You may also see any lint errors in the console.

![tampilan](./src/images/Screenshot%20(175).png)